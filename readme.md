# KumuluzEE Node.js Health

KumuluzEE Node.js Health is an open-source health check module. 

It is meant to provide similar functionality as its [Java counterpart](https://github.com/kumuluz/kumuluzee-health).
It is a flexible and customisable health check library developed for microservices. 

It is mainly developed for express, but should work with any similar framework. 
## Install

Node version >= 8.0.0:

```
$ npm install --save kumuluz-health
```

## Usage


```javascript
// -- INIT EXPRESS SERVER --
const express = require('express');
let app = express();

// -- INCLUDE THE MODULE --
let health = require("kumuluzee-health")()

// load your configuration
let options = {...}

// export your configuration so it is visible to the module
module.exports = {
    kumuluzHealthOptions: () => {
        return options
    }
};

// expose it on your server
app.use("/health", health);
```

## Configuration

Here we can setup built-in health checks, and setup logging. 

Example configuration:
``` json
{
  "data": [
    {
      "name": "Mongodb main",
      "type": "mongodb",
      "urls": [
        "mongodb://localhost:27017/tenants",
        "mongodb://127.0.0.1:27017/tenants"
      ],
      "requireAll": true
    },
    {
      "name": "Google search",
      "type": "http",
      "urls": [
        "http://google.com",
        "https://google.com"
      ],
      "required": false
    },
    {
      "name": "Google search ping",
      "type": "ping",
      "urls": [
        "google.com"
      ],
      "required": true
    },
    {
      "name": "Disk space",
      "type": "diskSpace",
      "limit": 100000,
      "required": true
    }
  ],
  "config": {
    "logs": {
      "enabled": true,
      "period": 30
    }
  }
}

```

Configuration is split into **data** and **config**

**data**: *required* - an array of healthcheck items
* **name** *required*  - name of the service. Used to easily differentiate services in results.
* **type** *required*  - one of default types (mongodb, http, ping, diskSpace) or name of custom function (see chapter "Custom checks")
* **urls** - unique locator
* **required** - is required one or more service 
* **requireAll** - are all services required 
* **urls** - unique locator
* **limit** - minimum limit of megabytes free on disk (only used in disk space check) 


**config**: - an array of healthcheck items
* **logs**:
    * **enabled** - boolean
    * **period** - interval of status logging in seconds. 


## Built-in checks
The module supports 4 built-in checks:

    mongodb - checks connection to the database. 
    ping - checks if the IP is accessible by ping
    http - checks if the http server returns a non-error http response
    diskSpace - checks disk space


## Custom checks

The module supports custom health checks. 
The module permits storing additional data. You should store it in this object, as another parameter (as shown in example - "customField").
``` json
{
  "data": [
    {
      "name": "Example custom function item",
      "type": "CoolExtraFunction",
      "urls": [
        "http://google.com",
        "https://google.com"
      ],
      "requireAll": true,
      "customField": "important data"
    },
    ...
  ]
}
```

Example for a HTTP check:
```javascript
const rp = require('request-promise');
// your custom function must return a promise (which returns a boolean).
let myCustomFunction = async (url) => {
    // url of the current item (they are provided individually) 
    // the whole data item is accessible via "this" - for example: this.customField
    let res = await rp({url: url, resolveWithFullResponse: true});
    return res.statusCode === 200;
};
// pass it as an object key (with the same name as in the configuration object) when require-ing the module.
let health = require("kumuluzee-health")({
    CoolExtraFunction: myCustomFunction
    // additional custom functions
})
//...
```


## Logging

The module logs the status of the server, time and relevant data or cause of the error.

It supports using custom logging functions, which are useful when sending logs to a logging server.

To use pass "log" AND "error" keys when require-ing the library.

If you do not use custom logging functions you can leave these fields empty (by default the module will use console.log and console.error).
```javascript
let health = require("kumuluzee-health")({
    log: console.log, 
    error: console.error, 
    CoolExtraFunction: myCustomFunction
})
//...
```

## License

MIT

