const rp = require('request-promise');
const healthCheckMongodb = require('health-check-mongodb');
const ping = require('qping');
const diskSpace = require('diskspace');
const os = require('os');


const diskSpacePath = os.type() === 'Windows_NT' ? 'C' : '/';

/*
    Standard checkAll object:
        { health: false,
          success: 1,
          error: 1,
          details:
           [ { name: 'mongodb://validUrl:41140/dbValid',
               health: true,
               message: '' },
             { name: 'mongodb://invalidUrl:41140/dbInvalid',
               health: false,
               message: 'getaddrinfo ENOTFOUND invalidUrl:41140' } ] }
*/


let generateDiskSpaceObj = (url, isAvailable) => {
    return {limit: url, state: isAvailable ? "UP" : "DOWN"}
};
let generateStatusObj = (url, isAvailable) => {
    return {url: url, state: isAvailable ? "UP" : "DOWN"}
};


let generateGeneralObj = (url, isAvailable, error) => {
    if (error)
        return {name: url, health: isAvailable, message: error};
    else
        return {name: url, health: isAvailable};
};

/*
    Accepts:
        - urls:
            An array of urls that will be checked
        - checkOne:
            An optional function that returns a promise (with true/false success results)
    Returns:
        - a standard checkAll object
 */
let generalCheckAll = async (urls, checkOne) => {
    let out = {
        health: false,
        success: 1,
        error: 0,
        details: []
    };

    for (let url of urls) {
        try {
            // TODO: parallel execution
            let res = await checkOne(url);
            if (res) {
                out.details.push(generateGeneralObj(url, true));
                out.health = true;
            } else {
                out.details.push(generateGeneralObj(url, false));
            }

        } catch (err) {
            out.details.push(generateGeneralObj(url, false));
            out.error = 1;
            out.success = 1;
        }
    }

    return out;
};

/*
    Accepts:
        - urls:
            An array of urls
        - checkAll:
            A generalCheckAll function with a custom checkOne function
            OR
            A custom checkAll function that ignores the second parameter (checkOne function)

            A custom checkAll function should return a standard checkAll object.
        - checkOne:
            Should be only passed when checkAll function is the generalOne.
            This function should accept a single url and return a promise that returns true or false - depending on whether the url is accessible.
            In case of an error the error will be displayed in "message" object parameter.
    Returns: an array of objects
        {
            hasError: false
            result: [{statusObject}]
        }
 */
let generalAvailability = async (urls, checkAll, checkOne) => {
    return customAvailability(urls, checkAll, checkOne, generateStatusObj)
    // let status = {
    //     hasError: false,
    //     result: []
    // };
    //
    // try {
    //     let res = await checkAll(urls, checkOne);
    //
    //     for (let detail of res.details)
    //         // TODO: change from statusObject to generalObject - and apply changes in processAvailability
    //         status.result.push(generateStatusObj(detail.name, detail.health));
    //     if (res.error > 0)
    //         status.hasError = true;
    //
    //     return status
    // } catch (err) {
    //     status.hasError = true;
    //     return status;
    // }
};
//{limit: detail.name, state: detail.health ? "UP" : "DOWN"}
let customAvailability = async (urls, checkAll, checkOne, generateDataObj) => {
    let status = {
        hasError: false,
        result: []
    };

    try {
        let res = await checkAll(urls, checkOne);

        for (let detail of res.details)
            status.result.push(generateDataObj(detail.name, detail.health));
        if (res.error > 0)
            status.hasError = true;

        return status
    } catch (err) {
        status.hasError = true;
        return status;
    }
};

/*
Example input:
{
    "hasError": true,
    "result": [
        {
            "url": "mongodb://localhost:27017/tenants",
            "state": "UP"
        },
   ]
}
 */
let processAvailability = (option, results) => {
    let out = {
        name: option.name,
        status: "UP",
        data: results.result
    };

    if (results.hasError) {
        out.status = "DOWN";
    } else {
        for (let result of results.result) {
            if (result.status !== "UP") {
                // if required field isn't set, then it should be treated as true
                if (!(option.required === false)) out.up = false;
                if (option.requireAll) out.up = false;
            }
        }
    }

    return out;
};

// --- CHECK ONE ---
let httpCheckOne = async (url) => {
    let res = await rp({url: url, resolveWithFullResponse: true});
    return res.statusCode === 200;
};
let pingCheckOne = async (url) => {
    return await ping.ping(url);
};
let diskSpaceCheckOne = async (limit) => {
    diskSpace.check(diskSpacePath, function (err, result) {
        return limit*1024*1024 <= result.free;
    });
};


// --- CHECK ALL ---
/*
    Accepts:
        - An array of URLs
        OR
        - disk: minimum disk space limit
    Returns:
        A standard object
 */
let mongoCheckAll = async (urls, _) => {
    urls = urls.map((obj) => {
        return {"url": obj}
    });
    return await healthCheckMongodb.do(urls);
};


// --- STANDARD CHECKS ---
let mongoCheck = async (options) => {
    let available = await generalAvailability(options.urls, mongoCheckAll);
    return processAvailability(options, available)
};

let httpCheck = async (options) => {
    let available = await generalAvailability(options.urls, generalCheckAll, httpCheckOne);
    return processAvailability(options, available)
};

let pingCheck = async (options) => {
    let available = await generalAvailability(options.urls, generalCheckAll, pingCheckOne);
    return processAvailability(options, available)
};

// minimum number of MBs free left on the main disk
let diskSpaceCheck = async (options) => {
    let available = await customAvailability([options.limit], generalCheckAll, diskSpaceCheckOne, generateDiskSpaceObj);
    return processAvailability(options, available)
};


// --- CUSTOM CHECK ---
// TODO: Write an example
let customCheck = async (options, customCheckOne) => {
    let available = await generalAvailability(options.urls, generalCheckAll, customCheckOne.bind(options));
    return processAvailability(options, available)
};

module.exports = {
    mongodb: mongoCheck,
    ping: pingCheck,
    http: httpCheck,
    diskSpace: diskSpaceCheck,
    custom: customCheck,
}
