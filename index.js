const express = require('express');
const router = express.Router();
const validation = require("./healthcheck");
const inputValidation = require("./inputValidation");
let {kumuluzHealthOptions} = require.main.exports;
let functions;

let getAvailability = async (options) => {
    let out = {
        outcome: "UP",
        checks: []
    };

    let res = {
        success: true
    };

    // TODO: parallel execution
    for (let option of options) {
        let status = false;
        switch (option.type) {
            case "mongodb":
                status = await validation.mongodb(option);
                break;
            case "http":
                status = await validation.http(option);
                break;
            case "ping":
                status = await validation.ping(option);
                break;
            case "custom":
                status = await validation.custom(option, functions[option.name]);
                break;
            case "diskSpace":
                status = await validation.diskSpace(option);
                break;
            default:
                console.error("Unexpected healthcheck type: " + option.type);
                break;
        }
        // do for every successful switch
        if (status) {
            out.checks.push(status);
            if (status.status === "DOWN") {
                out.outcome = "DOWN";
                res.success = false;
            }
        }

    }

    res.data = out;

    return res;
};

/*
example result
{
 "outcome": "UP",
 "checks": [
    {
      "name": "MyCheck",
      "state": "UP",
      "data: {
        "key": "value",
        "key1": "val1",
        "key2": "val2"
      }
    }
  ]
}
*/
let generateStatus = (req, res) => {
    let options = kumuluzHealthOptions().data;
    let valid = inputValidation(options);
    console.log("IS VALID", valid);
    if (!valid)
        return console.error("kumuluzee-health configuration is set incorrectly. The module will not work. Please check your configuration.");
    getAvailability(options)
        .then((result) => {
            if (result.success)
                res.status(200).send(result.data);
            else
                res.status(503).send(result.data);
        })
        .catch((err) => res.status(500).send(err));
};
let logStatus = (logger) => {
    let options = kumuluzHealthOptions().data;
    if (!logger || !logger.log || !logger.error)
        logger = console;
    getAvailability(options)
        .then((result) => {
            if (result.success)
                logger.log( JSON.stringify({source: "kumuluzee-health", time: new Date().toString(), status: 200, data: result.data}));
            else
                logger.log( JSON.stringify({source: "kumuluzee-health", time: new Date().toString(), status: 503, data: result.data}));
        })
        .catch((err) => {
            logger.error( JSON.stringify({source: "kumuluzee-health", time: new Date().toString(), status: 500, data: null, error:err}));
        })
};

let init = (functions) => {
    this.functions = functions;
    let options = kumuluzHealthOptions();
    if (! inputValidation(options)) throw new Error("Configuration validation failed. Health check will not work.");
    let {checks, config} = options;

    if (config.logs && config.logs.enabled || !config.logs) {
        let timeout = (config.logs.period || 30) * 1000;
        setInterval(function () {
            logStatus(functions)
        }, timeout);
    }

    router.get('/', generateStatus);
    return router
};

module.exports = function (options) {
    return init(options);
};


