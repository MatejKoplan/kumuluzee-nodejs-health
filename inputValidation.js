let Ajv = require('ajv');
let ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
let schema = require("./validationSchema")

let validate = (data) => {
    try {
        let validate = ajv.compile(schema);
        let valid = validate(data);
        if (!valid) console.error("Invalid input: ", validate.errors);
        return valid
        // return validationObjectSchema.isValidSync(data[0])
    }
    catch (e) {
        throw new Error("Input validation failed. Please check your input data. " + e);
    }
};

module.exports = validate;